## Synopsis

WebIDE is yet another [online IDE][1] that handles javascript libraries, css, html and javascript. It is far from complete as it was an experiment, but it progressed much farther than anyone had hoped in the exceptionally short period of time that it was worked on.
[1]: http://en.wikipedia.org/wiki/Online_Javascript_IDE

## Motivation

WebIDE is very similar to [jsfiddle][2]. It was built because jsfiddle did a page refresh whenever code changed (at least when this was built) and because proxies at work often cause problems with jsfiddle and running things locally helps circumvent the network problems.
[2]: http://www.jsfiddle.net/

## Installation

The code is checked into Bitbucket as an Eclipse project, but you should be able to get it going from any web container if you package the code as a war file.
The main url once your server is started up should be [myserver:port/WebIDE/][3]
[3]: localhost:8080/WebIDE/

## Highlights

1. The live updating of the code turned out very well. 
2. The general nod to the core functionality of fully functional online IDE's such as jsfiddle was fairly complete.
3. I wrote my own css parser/css auto-completion as a plug-in to [codemirror][4]. 
4. The dockable/undockable preview window was very cool although still a bit buggy in certain browsers.
[4]: http://www.codemirror.net

## Contributors

I wrote this partly as an exercise to see if I could clone most of the jsfiddle functionality as well as improve upon it. My main motivator was Grant Stephas who constantly complained about jsfiddle implementation, so I told him I would fix it.