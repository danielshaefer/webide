package org.shaeferdp.webide.model;

import java.util.HashMap;

public class User {

	public User()
	{
		
	}
	
	public User(int id)
	{
		userId = id;
	}
	
	private int userId;
	private HashMap<String, Project> versionsByKey = new HashMap<>();

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	public HashMap<String, Project> getVersionsByKey() {
		return versionsByKey;
	}

	public void setVersionsByKey(HashMap<String, Project> versionsByKey) {
		this.versionsByKey = versionsByKey;
	}
	
}
