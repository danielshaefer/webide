package org.shaeferdp.webide.model;

public class Project {

	private String key;
	private Integer version;
	private String code;
	private String name;
	public Project(String newKey, int version) {
		this.key = newKey;
		this.version = version;
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getKey() {
		return key;
	}
	public Integer getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
