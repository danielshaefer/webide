package org.shaeferdp.webide.model;

import javax.naming.InitialContext;
import javax.naming.NamingException;

public class Environment
{

   public static String getCurrentEnvironment()
   {
      try
      {
         return (String) (new InitialContext().lookup("java:comp/env/ENVIRONMENT_TYPE"));
      }
      catch (NamingException e)
      {
         e.printStackTrace();
         return "Could not determine environment.";
      }
   }
   
}
