package org.shaeferdp.webide.dao;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.sql.DataSource;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.context.support.ServletContextResource;

/**
 * This class provides a way to initialize and shutdown a database.  To initialize, it makes use
 * of the fact that it implements InitializingBean.  To shutdown the database, it implements
 * ApplicationListener and listens for a context shutdown.  We need the servlet context to
 * grab the SQL used in initialization, hence the ServletContextAware.
 */
public class H2Initializer  implements InitializingBean, ServletContextAware {
	@Autowired DataSource dataSource;
	List<String> locationList;
	ServletContext servletContext;

	Logger log = Logger.getLogger(this.getClass());

	/** InitializingBean implementation */
	public void afterPropertiesSet() throws Exception {
		if (this.dataSource == null) {
			throw new IllegalArgumentException("DataSource is required");
		}
		initializeDB();
	}

	/** Initialize the database by grabbing the SQL and executing it! */
	private void initializeDB() throws IOException 
	{
		if (locationList != null)
		{
			log.info("Attempting to retrieve initializing SQL FROM LIST");
			List<String> locations = locationList;
			JdbcTemplate jt = new JdbcTemplate(this.dataSource);
			for (String string : locations) {
				System.out.println("Resource string: " + string);
				Resource sql = getResource(string);
				try
				{
					jt.execute(IOUtils.toString(sql.getInputStream()));
					log.info("Successfully updated SQL"+string);
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
					log.info("Failed to update SQL: "+string);
				}
			}
		}
		else
			log.info("No location set, so not retrieving initializing SQL");
		
		try
		{
			postInitRoutine();
		}
		catch(Exception ex)
		{
			log.info("Failed to run postInit routine.");
		}
	}

	private void postInitRoutine() 
	{
		
	}

	protected Resource getResource(String location)
	{
		return new ServletContextResource(this.servletContext, location);
	}

	/** ServletContextAware implementation */
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
		
	}	
	public void setLocationList(List<String> locationList) {
		this.locationList = locationList;
	}
}
