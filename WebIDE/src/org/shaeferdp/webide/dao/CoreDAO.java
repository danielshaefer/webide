package org.shaeferdp.webide.dao;

import java.util.HashMap;
import java.util.Map;

import org.shaeferdp.webide.model.Project;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;


public class CoreDAO {

	@Autowired private JdbcTemplate jdbcTemplate;
	
	private Map<String, Project> projects = new HashMap<>();
	
	public Project getProjectByKey(String key)
	{
		return projects.get(key);
	}
	
	public boolean addProject(Project project)
	{
		projects.put(project.getKey(), project);
		return true;
	}
	public boolean saveProject(Project project)
	{
		int updated = jdbcTemplate.update("insert into webide_project (project_key, version, name, code) values (?, ? ,? , ?)", new Object[]{project.getKey(), project.getVersion(), project.getName(), project.getCode()});
		return updated > 0;
	}
	
}
