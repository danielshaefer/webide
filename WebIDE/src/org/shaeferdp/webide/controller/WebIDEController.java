package org.shaeferdp.webide.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.shaeferdp.webide.dao.CoreDAO;
import org.shaeferdp.webide.model.Project;
import org.shaeferdp.webide.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class WebIDEController
{
	
   private static Logger logger = Logger.getLogger(WebIDEController.class);
   @Autowired private CoreDAO coreDAO;
   @Autowired private User user;
   
   @RequestMapping(value = "/index", method = RequestMethod.GET)
   public ModelAndView index()
   {
	   logger.info("Test index hit");
      ModelAndView mav = new ModelAndView("index.jsp");
      return mav;
   }
   
   @RequestMapping(value = "/save", method = RequestMethod.POST)
   @ResponseBody
   public String save(String code, String name, String key) throws IOException
   {
	   System.out.println("Saving--starting with key: " + key);
	   Project project = validateKey(key);
	   project.setName(name);
	   System.out.println(project.getCode());
	   System.out.println(code);
	   if (project.getKey().equals(key) && !StringUtils.equals(project.getCode(),code)) //currently always versioning b/c version is included in code.
	   {
		   System.out.println("VERSIONING");
		   project.setCode(code);
		   project.setVersion(project.getVersion() + 1); //same key means new version. //only version if code has changed.
		   coreDAO.saveProject(project);
	   }
	   else
	   {
		   project.setCode(code);
	   }
	   coreDAO.addProject(project); //always keep up to date in map...but don't always save to database.
	   JSONObject json = JSONObject.fromObject(project);
	   return json.toString();
   }
   
   @RequestMapping(value = "/getFile")
   public void file(String key, HttpServletResponse resp) throws IOException
   {
	   Project p = coreDAO.getProjectByKey(key);
	   resp.setContentType("txt/plain");
	   resp.setHeader("Content-Disposition", "filename=" + key + "_" + p.getVersion() + ".txt");
	   IOUtils.copy(IOUtils.toInputStream(p.getCode()), resp.getOutputStream());
   }
   
   private Project validateKey(String key) {
	   User user = getUser();
	   if (user.getVersionsByKey().containsKey(key))
	   {
		   return user.getVersionsByKey().get(key);
	   }
	   else
	   {
		   System.out.println("GENERATED NEW KEY");
		   String newKey = generateKey();
		   Project p = new Project(newKey, 1);
		   user.getVersionsByKey().put(newKey, p);
		   return p;
	   }
   }

   private User getUser() {
	   if (user == null || user.getUserId() == 0)
		   user = new User(1);
	   return user;
   }


   
   private String generateKey()
   {
	   return RandomStringUtils.randomAlphanumeric(20);
   }

}
