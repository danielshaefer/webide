package org.shaeferdp.webide.util;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.shaeferdp.webide.model.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

public class Emailer
{
   private static Logger logger = Logger.getLogger(Emailer.class);
   private static String applicationFrom = "WebIDE@shaefer.org";
   private static String applicationTo = "shaefer@gmail.com";
   
   @Autowired private MailSender sender;
   
   public void sendMail(String to, String from, String subject, String body)
   {
      SimpleMailMessage mailMessage = new SimpleMailMessage();
         mailMessage.setTo(to);
         mailMessage.setFrom(from);
         mailMessage.setSubject(subject);
         mailMessage.setText(body);
         sender.send(mailMessage);
   }
   public void sendMailFromApplication(String to, String subject, String body)
   {
      sendMail(to, applicationFrom, subject, body);
   }
   public void sendMailFromApplicationToStandardRecipients(String subject, String body)
   {
      sendMail(applicationTo, applicationFrom, subject, body);
   }
   
}
