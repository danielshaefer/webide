<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="d" uri="http://displaytag.sf.net/el" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Web IDE</title>
<link href="css/normalize.css" rel="stylesheet" type="text/css">
<link href="css/layout.css" rel="stylesheet" type="text/css">
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/redmond/jquery-ui.css" rel="stylesheet" type="text/css">
<link href="css/jquery.snippet.min.css" rel="stylesheet" type="text/css">
<link href="css/codemirror/codemirror.css" rel="stylesheet" type="text/css">
<link href="css/codemirror/util/dialog.css" rel="stylesheet" type="text/css">
<link href="css/codemirror/util/simple-hint.css" rel="stylesheet" type="text/css">
<link href="css/codemirror/theme/cobalt.css" rel="stylesheet" type="text/css">
<link href="css/codemirror/theme/eclipse.css" rel="stylesheet" type="text/css">
<link href="css/codemirror/theme/elegant.css" rel="stylesheet" type="text/css">
<link href="css/codemirror/theme/monokai.css" rel="stylesheet" type="text/css">
<link href="css/codemirror/theme/neat.css" rel="stylesheet" type="text/css">
<link href="css/codemirror/theme/night.css" rel="stylesheet" type="text/css">
<link href="css/codemirror/theme/rubyblue.css" rel="stylesheet" type="text/css">
<link href="css/codemirror/codemirror_style.css" rel="stylesheet" type="text/css">
<link href="css/webideBase.css" rel="stylesheet" type="text/css">
</head>
<body>
		<div class="splashScreen"><img src="images/zephyrlogo.png"></div>
		<div class="splashScreenOverlay"></div>
		<form id="codeForm" action="./save.wi" method="post"><input id="key" name="key" type="hidden" value="${key }"></form>
		<div class="pane ui-layout-north">
			<img src="images/zephyrlogoimg_32.png" style="float:left;width:35px;margin-right:10px" title="Project Zephyr Image"><img src="images/zephyrlogotext.png" style="float:left;margin-right:10px;display:none" title="Project Zephyr">
			<button id="saveButton" style="float:left"><img src="images/size_16x16/save.png" class="buttonImg">Save</button>
			<button id="downloadButton" style="float:left;margin-left:10px"><img src="images/size_16x16/download.png" class="buttonImg">Download</button>
			<button id="newButton" style="float:left;margin-left:10px"><img src="images/size_16x16/newspaper_add.png" class="buttonImg">New</button>
			
			<button id="configButton" style="float:left;margin-left:25px"><img src="images/size_16x16/cog_edit.png" class="buttonImg">Config...</button>
			
			<button id="resetLayoutButton" style="float:right;margin-left:10px"><img src="images/size_16x16/table_refresh.png" class="buttonImg">Reset Layout</button>
			<button id="undockPreview" style="float:right"><img src="images/size_16x16/layout_delete.png" class="buttonImg">Undock Preview</button>
			<button id="dockPreview" style="float:right;display:none"><img src="images/size_16x16/layout_add.png" class="buttonImg">Dock Preview</button>
		</div>
		<div class="pane ui-layout-east">
			<div class="pane ui-east-layout-east">
			</div>
			<div class="pane ui-east-layout-center">
				<iframe id=preview name="previewFrame" width="100%" height="100%" frameborder="0" src="about:blank"></iframe>
			</div>
		</div>
		<div class="pane ui-layout-west">
			<div><span>Name: </span><input id="projectName"/></div>
			<div><span>Key: </span><span id="keyDisplay"></span></div>
			<div><span>Version: </span><span id="versionDisplay">1</span></div>
			<jsp:include page="config.jsp"></jsp:include>
		</div>
		<div class="pane ui-layout-south">
			<div><span>Downloaded files: </span><div id="fileList"></div></div>
		</div>
		<div class="pane ui-layout-center">
			<div class="pane ui-inner-layout-north">
				<div class="editorLabel">Html Body</div>
				<textarea class='htmlEditor codeEditor'></textarea>
			</div>
			<div class="pane ui-inner-layout-center">
				<div class="editorLabel">Javascript</div>
				<textarea class='javascriptEditor codeEditor'></textarea>
			</div>
			<div class="pane ui-inner-layout-south">
				<div class="editorLabel">CSS</div>
				<textarea class='cssEditor codeEditor'></textarea>
			</div>
		</div>

	
	<script src="script/ieConsoleEater.js" type="text/javascript"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js" type="text/javascript"></script>
	<script src="script/jquery.themeswitcher.js" type="text/javascript"></script>
	<script src="script/date.js" type="text/javascript"></script>
	<script src="script/jquery.layout-1.3.0.min.js" type="text/javascript"></script>
	<script src="script/jquery.hotkeys-0.7.9.min.js" type="text/javascript"></script>
	<script src="script/jquery.caret.1.02.min.js" type="text/javascript"></script>
	<script src="script/scrollableTabs/jquery.scrollabletab.min.js" type="text/javascript"></script>
	<script src="script/codemirror/codemirror.js" type="text/javascript"></script>
	<script src="script/codemirror/css.js" type="text/javascript"></script>
	<script src="script/codemirror/htmlmixed.js" type="text/javascript"></script>
	<script src="script/codemirror/javascript.js" type="text/javascript"></script>
	<script src="script/codemirror/xml.js" type="text/javascript"></script>
	<script src="script/codemirror/util/simple-hint.js" type="text/javascript"></script>
	<script src="script/codemirror/util/javascript-hint.js" type="text/javascript"></script>
	<script src="script/webide.js" type="text/javascript"></script>
	<script src="script/webide-save.js" type="text/javascript"></script>
	<script src="script/fileHandling.js" type="text/javascript"></script>
	<script src="script/docking.js" type="text/javascript"></script>
	<script src="script/groovyutils.js" type="text/javascript"></script>
	<script src="script/cssProperties_2.1.js" type="text/javascript"></script>
	<script src="script/cssAutoComplete_codemirror.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			console.warn('actual page ready');
			$('.splashScreen, .splashScreenOverlay').hide();
			
			$.webide.config.key = $('#key').val();
			$('#projectName').keyup(function(){
				$.webide.currentProjectName = $(this).val();
			});
			var htmlCodeMirror = CodeMirror.fromTextArea($('.htmlEditor')[0], {
				mode:"htmlmixed",
				theme:"eclipse",
				lineNumbers:true
			});
			var jsCodeMirror = CodeMirror.fromTextArea($('.javascriptEditor')[0], {
				mode:"javascript",
				theme:"eclipse",
				lineNumbers:true,
				extraKeys: {"Ctrl-Space": function(cm) {CodeMirror.simpleHint(cm, CodeMirror.javascriptHint);}}
			});
			var cssCodeMirror = CodeMirror.fromTextArea($('.cssEditor')[0], {
				mode:"css",
				theme:"eclipse",
				lineNumbers:true,
				extraKeys: {"Ctrl-Space": function(cm){cssAutoComplete(cm);}}
			});
			var srcCodeMirror = CodeMirror($('.ui-east-layout-east')[0], {
				  mode:  "htmlmixed",
				  theme:"eclipse"
				});
			setupAutoComplete(cssCodeMirror);
			var codeEditors = {css:cssCodeMirror, js:jsCodeMirror, html:htmlCodeMirror, src:srcCodeMirror};
			$.webide.codeEditors = codeEditors;

			loadExistingCodeEditors();
			  $('textarea').keyup(function(){$.typingActive = new Date()});
		      setInterval(function(){
		    	  if ( isIdle(0))
	    		  {
	    		  	updatePreview(codeEditors);
	    		  }
		      }, 300);
		      updatePreview(codeEditors); //update on page load.
		    $('#codeStyle').change(function(){
		    	console.warn($(this).val());
		    	codeEditors.html.setOption("theme", $(this).val()); 
		    	codeEditors.js.setOption("theme", $(this).val()); 
		    	codeEditors.css.setOption("theme", $(this).val()); 
		    });
		    $('.libVersion').change(function(){
		    	var newVersion = $(this).val();
		    	$.webide.config.setVersion($(this).attr("lib"), newVersion);
		    });
		    $('#jqueryuitheme').change(function(){
		    	var newTheme = $(this).val();
		    	$.webide.config.setTheme($(this).attr("lib"), newTheme);
		    });
		    /**
		    $('#pagejqueryuitheme').change(function(){
		    	var newTheme = $(this).val();
		    	$.webide.config.setPageTheme(newTheme);
		    });
		    **/
		    $('#themeswitcher').themeswitcher();
		    $('.libInclude').click(function(){
		    	var checked = $(this).is(':checked');
		    	$.webide.config.setInclude($(this).attr("lib"), checked);
		    });
		    
		    var outerLayout = $('body').layout({
		    	east:{
		    		size: '40%',
		    		onopen: function(){
		    			dock();
		    		}
		    	},
		    	south:{
		    		size: 50,
		    		initClosed: true,
		    		togglerTip_closed: "Open Download Panel",
		    		togglerTip_open: "Close Download Panel"
		    	},
		    	west:{
		    		togglerTip_closed: "Open Config Panel",
		    		togglerTip_open: "Close Config Panel",
		    		initClosed: false
		    	}
		    });
		    var innerLayout = $('.ui-layout-center').layout({
		    	north:{
		    		paneSelector:	".ui-inner-layout-north",
		    		size: '33%',
			    	padding: 0,
			    	togglerTip_closed: "Open HTML Panel",
			    	togglerTip_open: "Close HTML Panel"
		    	},
		    	center:{
		    		paneSelector:	".ui-inner-layout-center"
		    	},
		    	south:{
		    		paneSelector:	".ui-inner-layout-south",
			    	size: '33%',
			    	padding: 0,
			    	togglerTip_closed: "Open CSS Panel",
			    	togglerTip_open: "Close CSS Panel"
		    	}
		    	
				
		    });
		    var eastLayout = $('.ui-layout-east').layout({
		    	center:{
		    		paneSelector:	".ui-east-layout-center"
		    	},
		    	east:{
		    		paneSelector: ".ui-east-layout-east",
			    	size: '40%',
			    	togglerTip_closed: 'Open Preview Source Panel',
			    	togglerTip_open: 'Close Preview Source Panel',
			    	initClosed: true
		    	}
		    	
		    });
		    $.webide.eastLayout = eastLayout;
		    $.webide.outerLayout = outerLayout;
		    $.webide.innerLayout = innerLayout;
		    
		    $('#resetLayoutButton').button().click(function(){
		    	console.warn('resetting layout');
		    	eastLayout.sizePane("east", '40%');
		    	outerLayout.sizePane("east", '40%');
		    	eastLayout.close("east");

		    	
		    	outerLayout.open("west"); //config
		    	outerLayout.close("south"); //downloads
		    	innerLayout.open("north");
		    	innerLayout.open("south");
		    	outerLayout.open("east"); //preview
		    	innerLayout.sizePane("north", '33%');
		    	innerLayout.sizePane("south", '33%');
		    	
		    	
		    });
		    $('#undockPreview').button().click(function(){
		    	undock();
		    });
		    $('#dockPreview').button().click(function(){
		    	dock();
		    });
		    $('#configButton').button().click(function(){
		    	outerLayout.toggle("west");
		    });
		    $('#saveButton').button().click(function(){
		    	console.warn('from button click')
		    	save();
		    });
		    console.warn("BINDING SAVE");
		    $('body').bind('keydown', 'ctrl+s', function() {
		    	console.warn('from key press')
		    	save();
				$("#saveButton").toggleClass("ui-state-active", 100);
				$("#saveButton").toggleClass("ui-state-active", 100);
				return false;
			});
		    $('#newButton').button().click(function(){
		    	emptyStorage();
		    	$.webide.codeEditors.html.setValue(""); 
    			$.webide.codeEditors.css.setValue("");
    			$.webide.codeEditors.js.setValue("");
    			$.webide.config.currentProjectName = "";
    			updateKeyAndVersion({key:"", version:1});
    			$('#projectName').val("");
		    	updatePreview($.webide.codeEditors);
		    });
		    $('#downloadButton').button().click(function(){
		    	var submitObj = {
		    			name:$.webide.config.currentProjectName,
		    			config:$.webide.config, 
		    			html:$.webide.codeEditors.html.getValue(), 
		    			css:$.webide.codeEditors.css.getValue(),
		    			js:$.webide.codeEditors.js.getValue()
		    			};
		    	$.post('./save.wi', {code:JSON.stringify(submitObj), name:$.webide.currentProjectName, key:$.webide.config.key}, function(project){
		    		console.warn('Submitted code.');
		    		updateKeyAndVersion(project);
		    		var now = Date.now();
		    		$('<div><a href="./getFile.wi?key='+project.key+'" id="getFile">'+project.key+'_' +project.version +'</a><span style="margin-left:10px">Generated: '+now.toString('MM/dd/yyyy hh:mm:ss') +'</span></div>').prependTo($('#fileList'));
		    		outerLayout.open("south");
		    	}, 'json');
		    	
		    });
		});
	</script>
</body>
</html>