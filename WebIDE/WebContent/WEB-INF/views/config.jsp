<div id="configContainer" style="border:1px solid black;padding:5px">
	<span>Output (Preview) Config</span>
	<div>
		<span>Code Style: </span><select id="codeStyle">
			<option value="default">Default</option>
			<option value="cobalt">Cobalt</option>
			<option value="eclipse">Eclipse</option>
			<option value="elegant">Elegant</option>
			<option value="monokai">Monokai</option>
			<option value="neat">Neat</option>
			<option value="night">Night</option>
			<option value="rubyblue">Ruby Blue</option>
		</select>
	</div>
	<div>
		<span>Includes:</span>
		<div><input type="checkbox" checked="checked" id="normalizeCss"/>Normalize.css</div>
		<div><input type="checkbox" checked="checked" class="libInclude" lib="jquery"/>
			<select class="libVersion" lib="jquery">
				<option value="1.9.1">jQuery 1.9.1</option>
				<option value="1.8.3">jQuery 1.8.3</option>
				<option value="1.7.1">jQuery 1.7.1</option>
				<option value="1.6.4">jQuery 1.6.4</option>
				<option value="1.5.2">jQuery 1.5.2</option>
				<option value="1.4.4">jQuery 1.4.4</option>
			</select>
			<select id="jqueryuitheme" lib="jqueryuitheme">
				<option>base</option>
				<option>black-tie</option>
				<option>blitzer</option>
				<option>cupertino</option>
				<option>dark-hive</option>
				<option>dot-luv</option>
				<option>eggplant</option>
				<option>excite-bike</option>
				<option>flick</option>
				<option>hot-sneaks</option>
				<option>humanity</option>
				<option>le-frog</option>
				<option>mint-choc</option>
				<option>overcast</option>
				<option>pepper-grinder</option>
				<option>redmond</option>
				<option>smoothness</option>
				<option>south-street</option>
				<option>start</option>
				<option>sunny</option>
				<option>swanky-purse</option>
				<option>trontastic</option>
				<option>ui-darkness</option>
				<option>ui-lightness</option>
				<option>vader</option>
			</select>
		</div>
		<div><input type="checkbox" checked="checked" class="libInclude" lib="jqueryui"/>
			<select class="libVersion" lib="jqueryui">
				<option value="1.10.1">jQuery UI 1.10.1</option>
				<option value="1.9.2">jQuery UI 1.9.2</option>
				<option value="1.8.16">jQuery UI 1.8.16</option>
				<option value="1.8.15">jQuery UI 1.8.15</option>
				<option value="1.8.14">jQuery UI 1.8.14</option>
				<option value="1.8.13">jQuery UI 1.8.13</option>
				<option value="1.8.12">jQuery UI 1.8.12</option>
				<option value="1.8.11">jQuery UI 1.8.11</option>
				<option value="1.8.10">jQuery UI 1.8.10</option>
				<option value="1.8.9">jQuery UI 1.8.9</option>
				<option value="1.8.8">jQuery UI 1.8.8</option>
				<option value="1.8.7">jQuery UI 1.8.7</option>
				<option value="1.8.6">jQuery UI 1.8.6</option>
				<option value="1.8.5">jQuery UI 1.8.5</option>
				<option value="1.8.4">jQuery UI 1.8.4</option>

			</select>
		</div>
		<div><input type="checkbox" class="libInclude" lib="mootools"/>
			<select class="libVersion" lib="mootools">
				<option value="1.4.1">MooTools 1.4.1</option>
				<option value="1.3.2">MooTools 1.3.2</option>
			</select>
		</div>
		<div><input type="checkbox" class="libInclude" lib="dojo"/>
			<select class="libVersion" lib="dojo">
				<option value="1.6.1">Dojo 1.6.1</option>
				<option value="1.5.1">Dojo 1.5.1</option>
			</select>
		</div>
		<div><input type="checkbox" class="libInclude" lib="prototype"/>
			<select class="libVersion" lib="prototype">
				<option value="1.7.0.0">Prototype 1.7.0.0</option>
				<option value="1.6.1.0">Prototype 1.6.1.0</option>
			</select>
		</div>
	</div>
</div>
<div id="pageConfigContainer" style="border:1px solid black;padding:5px">
	<span>Page Config</span>
	<div id="themeswitcher"></div>
	<!-- <select id="pagejqueryuitheme">
		<option>base</option>
		<option>black-tie</option>
		<option>blitzer</option>
		<option>cupertino</option>
		<option>dark-hive</option>
		<option>dot-luv</option>
		<option>eggplant</option>
		<option>excite-bike</option>
		<option>flick</option>
		<option>hot-sneaks</option>
		<option>humanity</option>
		<option>le-frog</option>
		<option>mint-choc</option>
		<option>overcast</option>
		<option>pepper-grinder</option>
		<option>redmond</option>
		<option>smoothness</option>
		<option>south-street</option>
		<option>start</option>
		<option>sunny</option>
		<option>swanky-purse</option>
		<option>trontastic</option>
		<option>ui-darkness</option>
		<option>ui-lightness</option>
		<option>vader</option>
	</select> -->
</div>