<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>CSS Detection 2</title>
<link href="css/codemirror/util/simple-hint.css" rel="stylesheet" type="text/css">
</head>
<body>
	<textarea id="cssData" style="width:300px;height:300px" class='cssWindow'>.someCss
	{
		something:else;
	}</textarea>
	<div class="display"></div>
	<script src="script/ieConsoleEater.js" type="text/javascript"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js" type="text/javascript"></script>
	<script src="script/jquery.hotkeys-0.7.9.min.js" type="text/javascript"></script>
	<script src="script/jquery.caret.1.02.min.js" type="text/javascript"></script>
	<script src="script/cssProperties_2.1.js" type="text/javascript"></script>
	<script src="script/cssAutoComplete.js" type="text/javascript"></script>
	<script src="script/groovyutils.js" type="text/javascript"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		console.warn('page ready');
		setupAutoComplete($('#cssData'));

		$('.cssWindow').bind('keydown', 'ctrl+space', function() {
			console.warn('ctrl-space hit');
			var caret = $(this).caret();
			cssAutoComplete($('#cssData'));
				$('.display').html(caret.end);
			});
		});
	</script>
</body>
</html>