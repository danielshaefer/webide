$.currentPreviewContent = "";
$.typingActive = new Date();
$.webide = {};
$.webide.currentProjectName = "";
$.webide.config = {
	libs:['jquery', 'jqueryui', 'jqueryuitheme', 'mootools', 'prototype', 'dojo'],
	jquery:{include:true, version:'1.8.3', versionTemplate:'https://ajax.googleapis.com/ajax/libs/jquery/VERSION/jquery.min.js'},
	jqueryui:{include:true, version:'1.8.16', versionTemplate:'https://ajax.googleapis.com/ajax/libs/jqueryui/VERSION/jquery-ui.min.js'},
	jqueryuitheme:{include:true, version:'1.8.16', css:true, theme:'base', versionTemplate:'https://ajax.googleapis.com/ajax/libs/jqueryui/VERSION/themes/THEME/jquery-ui.css'},
	mootools:{include:false, version:'1.4.1', versionTemplate:'https://ajax.googleapis.com/ajax/libs/mootools/VERSION/mootools-yui-compressed.js'},
	prototype:{include:false, version:'1.7.0.0', versionTemplate:'https://ajax.googleapis.com/ajax/libs/prototype/VERSION/prototype.js'},
	dojo:{include:false, version:'1.6.1', versionTemplate:'https://ajax.googleapis.com/ajax/libs/dojo/VERSION/dojo/dojo.xd.js'},
	setVersion:function(lib, version){
		try
		{
			$.webide.config[lib].version = version;
			$.webide.config[lib].src = $.webide.config[lib].versionTemplate.replace("VERSION", version);
			if (lib == 'jqueryuitheme')
			{
				$.webide.config[lib].src = $.webide.config[lib].src.replace("THEME", $.webide.config[lib].theme);
				console.warn($.webide.config[lib].src);
			}
			if (lib == 'jqueryui')
				$.webide.config.setVersion('jqueryuitheme', version);
		}
		catch(ex)
		{
			console.warn('could not find lib: ' + lib + ' to set version: ' + version);
		}
	},
	setInclude:function(lib, checked){
		try
		{
			$.webide.config[lib].include = checked;
			if (lib == 'jquery')
				$.webide.config.setInclude('jqueryuitheme', checked);
		}
		catch(ex)
		{
			console.warn('could not find lib: ' + lib + ' to set include: ' + checked);
		}
	},
	setTheme:function(lib, theme)
	{
		$.webide.config[lib].theme = theme;
		$.webide.config[lib].src = $.webide.config[lib].versionTemplate.replace("VERSION",  $.webide.config[lib].version);
		$.webide.config[lib].src = $.webide.config[lib].src.replace("THEME", theme);
	},
	setPageTheme:function(theme)
	{
		var src = 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/THEME/jquery-ui.css'
		src =src.replace("THEME", theme);
	}
};
/**
 * For all libs setInitialVersion
 */
for(var i = 0;i<$.webide.config.libs.length;i++)
{
	var libLabel = $.webide.config.libs[i];
	var lib = $.webide.config[libLabel];
	if (lib)
	{
		$.webide.config.setVersion(libLabel, lib.version);
	}
}
function updatePreview(ce, forceUpdate) 
{	
	var previewFrame = document.getElementById('preview');
	var preview =  previewFrame.contentDocument ||  previewFrame.contentWindow.document;
	var nl = "\n";
	var t = "\t";
	var newContent  = "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">" + nl;
	newContent += "<html>" + nl;
	newContent += t + "<head>" + nl;
	newContent += t + t + "<title>Test Iframe</title>" + nl;
	if ($('#normalizeCss').is(":checked"))
		newContent += t + t + "<link href='http://"+location.host+"/WebIDE/css/normalize.css' rel='stylesheet' type='text/css'>" + nl;
	for(var i = 0;i<$.webide.config.libs.length;i++)
	{
		var libLabel = $.webide.config.libs[i];
		var lib = $.webide.config[libLabel];
		if (lib && lib.include && lib.css)
		{
			newContent += t + t + "<link href='" + lib.src + "' rel='stylesheet' type='text/css'/>" + nl;
		}
	}
	newContent += t + t + "<style type=\"text/css\">" + nl;
	newContent += t + t + t + ce.css.getValue() + nl;
	newContent += t + t +"<\/style>" + nl;
	newContent += t + "</head>" + nl;
	newContent += t + "<body>" + nl;
	newContent += t + t + ce.html.getValue() + nl;
	newContent += t + t + "<script src='http://"+location.host+"/WebIDE/script/ieConsoleEater.js' type='text/javascript'><\/script>" + nl;
	for(var i = 0;i<$.webide.config.libs.length;i++)
	{
		var libLabel = $.webide.config.libs[i];
		var lib = $.webide.config[libLabel];
		if (lib && lib.include && !lib.css)
		{
			newContent += t + t + "<script src=\""+lib.src+"\" type=\"text/javascript\"><\/script>" + nl;
		}
	}
	newContent += t + t + "<script type=\"text/javascript\">" + nl;
	if ($.webide.config.jquery.include)
		newContent += t + t + t + "$(document).ready(function(){" + nl; 
			newContent += t + t + t + t + ce.js.getValue() + nl;
	if ($.webide.config.jquery.include)
		newContent += t + t + t + "});" + nl;
	newContent += t + t + "<\/script>" + nl;
	newContent += t + "</body>" + nl;
	newContent += "</html>";
	if ($.currentPreviewContent != newContent || forceUpdate)
	{
		preview.open();
		preview.write(newContent);
		preview.close();
		
		if ($.browser.webkit) {
			preview.src = preview.src; //doing only this in FF does not cause the javascript to rerun...
		}
		else
		{
			preview.location.reload(true); //doing this in Chrome causes iframe to load a src = to the parent location src...looks like a bug to me.
		}
		$.currentPreviewContent = newContent;
		ce.src.setValue(newContent);
		
		if ($.webide.config.undock)
		{
			var windowPrefs = 'height=400,width=400,location=0,menubar=0,toolbar=0,status=0,titlebar=0';
			$.webide.previewWindow = $.webide.previewWindow || window.open('about:blank', '', windowPrefs, false);
			$.webide.previewWindow.document.open();
			$.webide.previewWindow.document.write(newContent + "");
			$.webide.previewWindow.document.close();
		}
	}
}
function isIdle(seconds)
{
	var now = new Date();
	var typingActive = $.typingActive;
	if (now.getTime() - typingActive.getTime() > seconds * 1000)
	{
		return true;
	}
	return false;
}
