function cssAutoComplete(cm)
{
	console.warn(cm);
   //console.warn("Jquery version: " + jQuery.fn.jquery, "Has caret plugin: " + (textField.caret() != undefined), "Has hotkeys plugin version: " + hotkeys.version);
   var dependencies = ["jquery", "caret", "hotkeys", "groovyutils", "simplehint.css", "cssProperties_2.1"]
   //var cssRegex = new RegExp("([\\.\\w-]+):?([\\w-\\s#!%\\(\\)]+)?", "gm");
   //var cssRegex = new RegExp("([\\w.-]+ *):?([^:;]+;)?", "gm");
   var cssRegex = new RegExp("(([\\w-.]+ *):([^:;\\s]+;?)?|([\\w-.]+ *))", "gm");
   var cssDefsRegex = new RegExp("{([\\s\\S]*?)}", "gm");
   var cssPropValReg = new RegExp("([.\\w-]+):?([\\w\\s-]+)?;?", "gm");
   var input = cm.getValue();
   console.debug(input);
   var matches = cssPropValReg.exec(input);
   
//    var defMatches = cssDefsRegex.exec(input);
//    var cssBlocks = [];
//    while(defMatches != null)
//    {
//        console.warn('FOUND CSS BLOCK: ' + defMatches[0]);
//        cssBlocks.push(defMatches[1]);
//        defMatches = cssDefsRegex.exec(input);
//    }
//    console.warn("CSS BLOCKS", cssBlocks)
//    var defs = [];
//    $(cssBlocks).each(function(){
//        var items = this.split("\n");
//        $(items).each(function(){
//            var trimmed = $.trim(this);
//            if (trimmed != "")
//                defs.push(trimmed);
//        });
//    });
//    console.warn("CSS BLOCKS TO DEFS", defs);
   var cssDefs = [];
   while(matches != null)
   {
       var startCarets = findIndicesOf(input, matches[0]);
       var matchData = [];
       for(var i = 0;i<startCarets.length;i++)
       {
           var startCaret = startCarets[i];
           var matchDataItem = getCssPropertyValueAndCaret(startCaret, matches[0]);
           matchDataItem.cmPosStart = cm.posFromIndex(matchDataItem.matchCaret.start);
           matchDataItem.cmPosEnd = cm.posFromIndex(matchDataItem.matchCaret.end);
           if (matchDataItem.matchCaret.colon != null)
        	   matchDataItem.cmPosColon = cm.posFromIndex(matchDataItem.matchCaret.colon);
           else
        	   matchDataItem.cmPosColon = null;
           if (matchDataItem.cssProperty[0] != ".") //couldn't find nice way to exclude css def names so I included the . in the regex and then remove them myself here.
               matchData.push(matchDataItem);
       }
      
       if (matchData.length > 0)
    	   cssDefs[cssDefs.length] = {match:matches, matchData:matchData};
       matches = cssPropValReg.exec(input);
   }

   var autocompleted = false;
   console.warn(cm.getCursor());
   console.warn(cm.lineInfo(cm.getCursor().line).text);
   //console.warn(cm.getSelection()); //actual selection
   //console.warn(cm.cursorCoords()); //actual cursor coors
   console.debug(cssDefs);
   var currentCursorPosition = cm.getCursor();
   $(cssDefs).each(function(){
       for(var x = 0;x<this.matchData.length;x++)
       {
           var matchData = this.matchData[x];
           var curMatch = this.match[0];
           var end = matchData.matchCaret.end;
           if (curMatch.charAt(curMatch.length-1) == ";")
               end = end - 1;
           var currentCursorCount = convertLinePosToIndex(currentCursorPosition);
           if (currentCursorCount >= convertLinePosToIndex(matchData.cmPosStart) && currentCursorCount <= convertLinePosToIndex(matchData.cmPosEnd) + 1)
           {
        	   console.debug("Caret position IS within css definition",currentCursorCount,convertLinePosToIndex(matchData.cmPosStart),convertLinePosToIndex(matchData.cmPosEnd), currentCursorCount >= convertLinePosToIndex(matchData.cmPosStart), currentCursorCount <= convertLinePosToIndex(matchData.cmPosEnd) + 1);
               var curCssProperty = $.trim(matchData.cssProperty);
               var curCssValue = $.trim(matchData.cssValue).replace("\n", "");
               console.debug("Ctrl-space was hit inside a css definition: " + this.match[0]);
               if (matchData.matchCaret.colon == null || currentCursorCount <= convertLinePosToIndex(matchData.cmPosColon))
               {
                   console.debug("COMPLETING CSS PROPERTY: " + curCssProperty + " Current VALUE: " + curCssValue);
                   var matchingProps = [];
                   for(var i = 0;i<cssProps.length;i++)
                   {
                       var cssprop = cssProps[i];
                       var regCss = new RegExp(curCssProperty + "", "ig");
                       if (cssprop.match(regCss) != null)
                       {
                           matchingProps[matchingProps.length] = cssprop;
                       }
                   }
                   this.matchData = matchData;
                   createOptionSelectAtCaretPosition(cm.cursorCoords(), matchingProps.sort(), this, curCssProperty,  "PROPERTY");
                   autocompleted = true;
               }
               else
               {
                   console.debug("COMPLETING CSS VALUE: " + curCssValue + " Current Prop: " + curCssProperty);
                   if (!css[curCssProperty])
                       return false;;
                   
                   //TODO: Replace certain values with additional options. i.e. (colors) with a bunch of color opts.
                   var valsToUse = convertPropertyPlaceholders(curCssProperty);
                   this.matchData = matchData;
                   createOptionSelectAtCaretPosition(cm.cursorCoords(), valsToUse, this, curCssValue,  "VALUE");
                   autocompleted = true;
               }
               return false;
           }
           else
           {
               console.debug("Caret position not within css definition",currentCursorCount,convertLinePosToIndex(matchData.cmPosStart),convertLinePosToIndex(matchData.cmPosEnd), currentCursorCount >= convertLinePosToIndex(matchData.cmPosStart), currentCursorCount <= convertLinePosToIndex(matchData.cmPosEnd) + 1);
           }
       }
       if (!autocompleted)
       {
           console.debug("DID ANY AUTOCOMPLETE: " + autocompleted);
           //if within css def braces autocomplete new prop
           this.matchData = {matchCaret:{colon:null,end:null}, cmPosEnd:cm.getCursor(), cmPosColon:null};
           createOptionSelectAtCaretPosition(cm.cursorCoords(), cssProps.sort(), this, "",  "PROPERTY");
       }
   });


}


function convertLinePosToIndex(cursor)
{
	return (cursor.line * 5000) + cursor.ch;
}

function convertPropertyPlaceholders(curCssProperty)
{
	var csspropvals = css[curCssProperty].vals;
	csspropvals.sort();
	var valsToUse = [];
    for(var c = 0;c<csspropvals.length;c++)
    {
        var val = csspropvals[c];
        if (val == "(color)")
        {
            valsToUse = valsToUse.concat(cssColors);
        }
        else if (val == "(length)")
        {
        	valsToUse = valsToUse.concat(cssLength);
        }
        else
        {
            valsToUse.push(val);
        }
    }
    return valsToUse;
}

function getCssPropertyValueAndCaret(startCaret, match)
{
   var endCaret = startCaret + match.length;
   var colonPos = match.indexOf(":");
   var colonCaret = null;
   var cssProperty = null;
   var cssValue = null;
   if (colonPos == -1)
   {
       cssProperty = match;
       cssValue = "";
       colonCaret = null;
   }
   else
   {
       colonCaret = colonPos + startCaret;
       cssProperty = match.substring(0, colonPos);
       cssValue = match.substring(colonPos+1, match.length);
   }
   var matchCaret = {start:startCaret, end:endCaret, colon:colonCaret};
   return {matchCaret:matchCaret, cssProperty:cssProperty, cssValue:cssValue};
}

function setupAutoComplete(cm)
{
	console.warn(cm);
   var div = $('#optsContainer');
   var select = $('#optsSelect');
   div = $("<div id='optsContainer' style='display:none'></div>");
   select = $("<select id='optsSelect' size='7' selectedIndex='0'></select>");
   div.append(select);
   $('body').append(div);
   
   select.click(function(){
       console.warn('click on autocomplete');
       var item = $('#optsSelect');
       console.warn(item.val());
       cssValOptionSelected(cm, item);
       return false;
   });
   select.bind('keydown', 'Return', function() {
       console.warn('enter hit on autocomplete');
       var item = $('#optsSelect');
       console.warn(item.val());
       cssValOptionSelected(cm, item);
       return false;
   });
   select.keydown(function(ev){
	   if (ev.keyCode != 13 && ev.keyCode != 40 && ev.keyCode != 38) //allow arrow keys and enter key...everything else cancels
	   {
		   $('#optsContainer').hide();
	       cm.focus();
	   }
   });
   $(document).bind('keydown', 'esc', function() {
       console.warn('esc clears autocomplete');
       $('#optsContainer').hide();
       cm.focus();
   });
   
   var inputHolder = $("<div id='additionalInputCss' style='display:none'></div>").css({
	   position: "absolute",
	   top: "0px",
	   left: "0px"
   });
   var input = $('<input id="additionalInput">');
   var goButton = $("<button>Go</button>").button().click(function(){handleAdditionalInputSubmit(cm);});
   inputHolder.append(input).append(goButton).appendTo($('body'));
   
  $('#additionalInputCss').bind('keydown', 'Return', function(){handleAdditionalInputSubmit(cm);});
   
}

function cssValOptionSelected(cm, item)
{
   
   var val = item.val();
   if (val != null && val[0] != "(")
   {
       var data = item.data("handlerData");
       var cssDef = data.cssDef;
       var propertyToReplace = data.propertyToReplace;
       var replaceType = data.replaceType;
       handleReplaceAndCaretPos(cm, val, cssDef, propertyToReplace, replaceType);
   }
   else if (val != null && val[0] == "(")
   {
       if (ArrayUtils.contains(cssAdditionalData, val))
       {
    	   var cursorCoords = cm.cursorCoords();
           var data = item.data("handlerData");
           var propertyToReplace = data.propertyToReplace;
           var input = $('#additionalInput');
           input.val(removeTypeLabels(item, propertyToReplace)).data("handlerData", data);
           var inputHolder = $("#additionalInputCss");
           inputHolder.css({
               position: "absolute",
               top: cursorCoords.y + "px",
               left: cursorCoords.x + "px",
               zIndex: 1
            });
           inputHolder.show();
           if (!isEntirelyVisible(inputHolder))
           {
           	repositionForVisibility(inputHolder, cursorCoords);
           }
           input.focus();
           $('#optsContainer').hide();
       }
       else
       {
    	   console.warn('need to handle: ' + val);
       }
   }
}

function removeTypeLabels(item, propertyToReplace)
{
	$(item).find("option").each(function(){
		var val = $(this).val();
		if (ArrayUtils.contains(cssAdditionalData, val))
		{
			var typeData = convertType(val);
			if (typeData.type == 'suffix')
				propertyToReplace = propertyToReplace.replace(typeData.data, "");
			else if (typeData.type == 'both')
			{
				propertyToReplace = propertyToReplace.replace(typeData.data.prefix, "");
				propertyToReplace = propertyToReplace.replace(typeData.data.suffix, "");
			}
		}
	});
	return propertyToReplace;
}

function handleAdditionalInputSubmit(cm)
{
	console.warn('handling additional submit');
    var item = $('#optsSelect');
	var type = convertType(item.val());
    var data = $('#additionalInput').data("handlerData");
    var cssDef = data.cssDef;
    var propertyToReplace = data.propertyToReplace;
    var replaceType = data.replaceType;
    var newVal = $('#additionalInput').val();
    newVal = removeTypeLabels(item, newVal); //handles is user types in more than we need...even though we strip this off at beginning we only do that to not give ppl the impression they need all that extra stuff...
    if (type.type == 'suffix')
    	newVal += type.data;
    else if (type.type == 'prefix')
    	newVal = type.data + newVal;
    else
    	newVal = type.data.prefix + newVal + type.data.suffix;
    handleReplaceAndCaretPos(cm, newVal, cssDef, propertyToReplace, replaceType);
    $('#additionalInputCss').hide();
}

function convertType(placeholder)
{
	var type = "suffix";
	placeholder = placeholder.replace("(", "").replace(")", "");
   if (placeholder == 'percent')
       return {type:type, data:"%"};
   if (placeholder == 'number' || placeholder == 'integer')
	   return {type:type, data:""};
   if (placeholder == 'shape')
   {
	   type = "both";
	   placeholder = {prefix:"rect(", suffix:")"};
   }
   else if (placeholder == 'string')
   {
	   type = "both";
	   placeholder = {prefix:"\"", suffix:"\""};
   }
   else if (placeholder == 'counter' || placeholder == 'attr' || placeholder == 'url')
   {
	   type = "both";
	   placeholder = {prefix:placeholder +"(", suffix:")"};
   }
   return {type:type, data:placeholder};
}

function createOptionSelectAtCaretPosition(cursorCoords, opts, cssDef, propertyToReplace, replaceType)
{
   if (opts.length == 0)
   {
       console.debug("No possible completions for: "+ propertyToReplace);
       return;
   }
   if (opts.length == 1 && opts[0] == propertyToReplace)
   {
       console.debug("One completion but it already matches: "+ propertyToReplace);
       return;
   }
   var div = $('#optsContainer');
   var select = $('#optsSelect');
   select.find("option").remove();
   
   for(var i = 0;i<opts.length;i++)
   {
       var option = $("<option>" + opts[i] + "</option>");
       if (i == 0)
           option.attr("selected", "selected");
       select.append(option);
   }
    select.data("handlerData", {cssDef:cssDef, propertyToReplace:propertyToReplace, replaceType:replaceType});
   
    div.css({
       position: "absolute",
       top: cursorCoords.y + "px",
       left: cursorCoords.x + "px"
    }).addClass("CodeMirror-completions").show();
    
    if (!isEntirelyVisible(div))
    {
    	repositionForVisibility(div, cursorCoords);
    }
    
    $(select).focus();
}

function handleReplaceAndCaretPos(cm, val, cssDef, propertyToReplace, replaceType)
{
   console.debug("FIRING HANDLEREPLACE", cm, val, cssDef, propertyToReplace, replaceType);
   //TODO: This will currently not properly handle multiple defs per line.
   if (replaceType == 'PROPERTY')
   {
       cm.focus();
       var curLine = cm.getLine(cm.getCursor().line);
       if (propertyToReplace == "")
       {
    	   cm.replaceSelection(val + ":");
       }
       else
       {
    	   var replacement = curLine.replace(propertyToReplace, val);
    	   if (cssDef.matchData.cmPosColon == null)
        	   replacement += ":";
           cm.setLine(cm.getCursor().line, replacement);
       }
       
   }
   else
   {
       cm.focus();
       var curLine = cm.getLine(cm.getCursor().line);
       console.warn("curLine: " + curLine);
       var curLineAfterColon = curLine.substring(curLine.indexOf(":")+1, curLine.length);
       console.warn("curLineAfterColon: " + curLineAfterColon);
       var newLineAfterColon = curLineAfterColon.replace(propertyToReplace, val);
       console.warn("newLineAfterColon: " + newLineAfterColon);
       if (newLineAfterColon[newLineAfterColon.length-1] != ';')
    	   newLineAfterColon += ";";
       var replacement = curLine.replace(":" + curLineAfterColon, ":" + newLineAfterColon);
       cm.setLine(cm.getCursor().line, replacement);
   }
   $('#optsContainer').hide();
   cm.focus();
}

function findIndicesOf(input, search)
{
   var indices = [];
   while(input.lastIndexOf(search) > -1)
   {
       var index = input.lastIndexOf(search);
       indices[indices.length] = index;
       input = input.substring(0, index);
   }
   return indices.reverse();
}

function getEntirelyVisible(el, objReturn)
{
	var off = $(el).offset();
	var t = off.top;
	var l = off.left;
	var h = $(el).height();
	var w = $(el).width();
	var docH = $(window).height();
	var docW = $(window).width();

	var bottom = t + h < docH;
	var top = t > 0;
	var left = l > 0;
	var right = l+ w < docW;
	var isEntirelyVisible = (top && left && bottom && right);
	var visible = {offTop:!top, offBottom:!bottom, offLeft:!left, offRight:!right, entirelyVisible:isEntirelyVisible};
	if (objReturn)
		return visible;
	return isEntirelyVisible;
}

function isEntirelyVisible(el)
{
	return getEntirelyVisible(el, false);
}

function repositionForVisibility(el, tlCoords)
{
	var el = $(el);
	var vis = getEntirelyVisible(el, true);
	console.debug("Visibility: ", vis);
	if (vis.offBottom && vis.offRight)
	{
		el.css({
			position: "absolute",
			top: tlCoords.y - el.height() + "px",
			left: tlCoords.x - el.width() + "px"
		});
	}
	else if (vis.offBottom)
	{
		el.css({
			position: "absolute",
			top: tlCoords.y - el.height() + "px",
			left: tlCoords.x + "px"
		});
	}
	else if (vis.offRight)
	{
		el.css({
			position: "absolute",
			top: tlCoords.y + "px",
			left: tlCoords.x - el.width() + "px"
		});
	}
}