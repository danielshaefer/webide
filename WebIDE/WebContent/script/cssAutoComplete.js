function cssAutoComplete(textField)
{
   console.warn("Jquery version: " + jQuery.fn.jquery, "Has caret plugin: " + (textField.caret() != undefined), "Has hotkeys plugin version: " + hotkeys.version);
   var dependencies = ["jquery", "caret", "hotkeys", "groovyutils", "simplehint.css", "cssProperties_2.1"]
   var textField = $(textField);
   //var cssRegex = new RegExp("([\\.\\w-]+):?([\\w-\\s#!%\\(\\)]+)?", "gm");
   //var cssRegex = new RegExp("([\\w.-]+ *):?([^:;]+;)?", "gm");
   var cssRegex = new RegExp("(([\\w-.]+ *):([^:;\\s]+;?)?|([\\w-.]+ *))", "gm");
   var cssDefsRegex = new RegExp("{([\\s\\S]*?)}", "gm");
   var cssPropValReg = new RegExp("([\\w-]+):?([\\w-]+)?;?", "gm");
   var input = textField.val();
   var matches = cssRegex.exec(input);
   
//    var defMatches = cssDefsRegex.exec(input);
//    var cssBlocks = [];
//    while(defMatches != null)
//    {
//        console.warn('FOUND CSS BLOCK: ' + defMatches[0]);
//        cssBlocks.push(defMatches[1]);
//        defMatches = cssDefsRegex.exec(input);
//    }
//    console.warn("CSS BLOCKS", cssBlocks)
//    var defs = [];
//    $(cssBlocks).each(function(){
//        var items = this.split("\n");
//        $(items).each(function(){
//            var trimmed = $.trim(this);
//            if (trimmed != "")
//                defs.push(trimmed);
//        });
//    });
//    console.warn("CSS BLOCKS TO DEFS", defs);
   var cssDefs = [];
   while(matches != null)
   {
       var startCarets = findIndicesOf(input, matches[0]);
       var matchData = [];
       for(var i = 0;i<startCarets.length;i++)
       {
           var startCaret = startCarets[i];
           var matchDataItem = getCssPropertyValueAndCaret(startCaret, matches[0]);
           if (matchDataItem.cssProperty[0] != ".") //couldn't find nice way to exclude css def names so I included the . in the regex and then remove them myself here.
               matchData.push(matchDataItem);
       }
       
       cssDefs[cssDefs.length] = {match:matches, matchData:matchData};
       matches = cssRegex.exec(input);
   }

   var autocompleted = false;
   var currentCaretPosition = textField.caret().end;
   $(cssDefs).each(function(){
       for(var x = 0;x<this.matchData.length;x++)
       {
           var matchData = this.matchData[x];
           var curMatch = this.match[0];
           var end = matchData.matchCaret.end;
           if (curMatch.charAt(curMatch.length-1) == ";")
               end = end - 1;
           if (currentCaretPosition >= matchData.matchCaret.start && currentCaretPosition <= end)
           {
               var curCssProperty = $.trim(matchData.cssProperty);
               var curCssValue = $.trim(matchData.cssValue).replace("\n", "");
               console.debug("Ctrl-space was hit inside a css definition: " + this.match[0]);
               if (matchData.matchCaret.colon == null || currentCaretPosition <= matchData.matchCaret.colon)
               {
                   console.debug("COMPLETING CSS PROPERTY: " + curCssProperty + " Current VALUE: " + curCssValue);
                   var matchingProps = [];
                   for(var i = 0;i<cssProps.length;i++)
                   {
                       var cssprop = cssProps[i];
                       var regCss = new RegExp(curCssProperty + "", "ig");
                       if (cssprop.match(regCss) != null)
                       {
                           matchingProps[matchingProps.length] = cssprop;
                       }
                   }
                   this.matchData = matchData;
                   createOptionSelectAtCaretPosition(textField, matchingProps.sort(), this, curCssProperty,  "PROPERTY");
                   autocompleted = true;
               }
               else
               {
                   console.debug("COMPLETING CSS VALUE: " + curCssValue + " Current Prop: " + curCssProperty);
                   if (!css[curCssProperty])
                       break;
                   
                   //TODO: Replace certain values with additional options. i.e. (colors) with a bunch of color opts.
                   var valsToUse = convertPropertyPlaceholders(curCssProperty);
                   this.matchData = matchData;
                   createOptionSelectAtCaretPosition(textField, valsToUse, this, curCssValue,  "VALUE");
                   autocompleted = true;
               }
               break;
           }
           else
           {
               console.debug("Caret position not within css definition");
           }
       }
       if (!autocompleted)
       {
           console.debug("DID ANY AUTOCOMPLETE: " + autocompleted);
           //if within css def braces autocomplete new prop
           this.matchData = {matchCaret:{colon:null,end:textField.caret().end}};
           createOptionSelectAtCaretPosition(textField, cssProps.sort(), this, "",  "PROPERTY");
       }
   });


}

function convertPropertyPlaceholders(curCssProperty)
{
	var csspropvals = css[curCssProperty].vals;
	csspropvals.sort();
	var valsToUse = [];
    for(var c = 0;c<csspropvals.length;c++)
    {
        var val = csspropvals[c];
        if (val == "(color)")
        {
            valsToUse = valsToUse.concat(cssColors);
        }
        else if (val == "(length)")
        {
        	valsToUse = valsToUse.concat(cssLength);
        }
        else
        {
            valsToUse.push(val);
        }
    }
    return valsToUse;
}

function getCssPropertyValueAndCaret(startCaret, match)
{
   var endCaret = startCaret + match.length;
   var colonPos = match.indexOf(":");
   var colonCaret = null;
   var cssProperty = null;
   var cssValue = null;
   if (colonPos == -1)
   {
       cssProperty = match;
       cssValue = "";
       colonCaret = null;
   }
   else
   {
       colonCaret = colonPos + startCaret;
       cssProperty = match.substring(0, colonPos);
       cssValue = match.substring(colonPos+1, match.length);
   }
   var matchCaret = {start:startCaret, end:endCaret, colon:colonCaret};
   return {matchCaret:matchCaret, cssProperty:cssProperty, cssValue:cssValue};
}

function setupAutoComplete()
{
   var div = $('#optsContainer');
   var select = $('#optsSelect');
   div = $("<div id='optsContainer' style='display:none'></div>");
   select = $("<select id='optsSelect' size='7' selectedIndex='0'></select>");
   div.append(select);
   $('body').append(div);
   
   select.click(function(){
       console.warn('click on autocomplete');
       var item = $('#optsSelect');
       console.warn(item.val());
       cssValOptionSelected(item);
       return false;
   });
   select.bind('keydown', 'Return', function() {
       console.warn('enter hit on autocomplete');
       var item = $('#optsSelect');
       console.warn(item.val());
       cssValOptionSelected(item);
       return false;
   });
   select.keydown(function(ev){
	   if (ev.keyCode != 13 && ev.keyCode != 40 && ev.keyCode != 38) //allow arrow keys and enter key...everything else cancels
	   {
		   $('#optsContainer').hide();
	       $('#cssData').focus();
	   }
   });
   $(document).bind('keydown', 'esc', function() {
       console.warn('esc clears autocomplete');
       $('#optsContainer').hide();
       $('#cssData').focus();
   });
   
   var textField = $('#cssData');
   var pos = $(textField).position();
   var width = $(textField).outerWidth();
   var inputHolder = $("<div id='additionalInputCss' style='display:none'></div>").css({
	   position: "absolute",
	   top: pos.top + "px",
	   left: (pos.left + width) + "px"
   });
   var input = $('<input id="additionalInput">');
   var goButton = $("<button>Go</button>").button().click(handleAdditionalInputSubmit);
   inputHolder.append(input).append(goButton).appendTo($('body'));
   
  $('#additionalInputCss').bind('keydown', 'Return', handleAdditionalInputSubmit);
   
}

function cssValOptionSelected(item)
{
   
   var val = item.val();
   if (val != null && val[0] != "(")
   {
       var textField = $('#cssData');
       var data = item.data("handlerData");
       var cssDef = data.cssDef;
       var propertyToReplace = data.propertyToReplace;
       var replaceType = data.replaceType;
       handleReplaceAndCaretPos(textField, val, cssDef, propertyToReplace, replaceType);
   }
   else if (val != null && val[0] == "(")
   {
       if (ArrayUtils.contains(cssAdditionalData, val))
       {
           var data = item.data("handlerData");
           var propertyToReplace = data.propertyToReplace;
           var input = $('#additionalInput');
           input.val(removeTypeLabels(item, propertyToReplace)).data("handlerData", data);
           $('#additionalInputCss').show();
           input.focus();
           $('#optsContainer').hide();
       }
       else
       {
    	   console.warn('need to handle: ' + val);
       }
   }
}

function removeTypeLabels(item, propertyToReplace)
{
	$(item).find("option").each(function(){
		var val = $(this).val();
		if (ArrayUtils.contains(cssAdditionalData, val))
		{
			var typeData = convertType(val);
			if (typeData.type == 'suffix')
				propertyToReplace = propertyToReplace.replace(typeData.data, "");
			else if (typeData.type == 'both')
			{
				propertyToReplace = propertyToReplace.replace(typeData.data.prefix, "");
				propertyToReplace = propertyToReplace.replace(typeData.data.suffix, "");
			}
		}
	});
	return propertyToReplace;
}

function handleAdditionalInputSubmit()
{
	console.warn('handling additional submit');
    var item = $('#optsSelect');
	var type = convertType(item.val());
    var textField = $('#cssData');
    var data = $('#additionalInput').data("handlerData");
    var cssDef = data.cssDef;
    var propertyToReplace = data.propertyToReplace;
    var replaceType = data.replaceType;
    var newVal = $('#additionalInput').val();
    newVal = removeTypeLabels(item, newVal); //handles is user types in more than we need...even though we strip this off at beginning we only do that to not give ppl the impression they need all that extra stuff...
    if (type.type == 'suffix')
    	newVal += type.data;
    else if (type.type == 'prefix')
    	newVal = type.data + newVal;
    else
    	newVal = type.data.prefix + newVal + type.data.suffix;
    handleReplaceAndCaretPos(textField, newVal, cssDef, propertyToReplace, replaceType);
    $('#additionalInputCss').hide();
}

function convertType(placeholder)
{
	var type = "suffix";
	placeholder = placeholder.replace("(", "").replace(")", "");
   if (placeholder == 'percent')
       return {type:type, data:"%"};
   if (placeholder == 'number' || placeholder == 'integer')
	   return {type:type, data:""};
   if (placeholder == 'shape')
   {
	   type = "both";
	   placeholder = {prefix:"rect(", suffix:")"};
   }
   else if (placeholder == 'string')
   {
	   type = "both";
	   placeholder = {prefix:"\"", suffix:"\""};
   }
   else if (placeholder == 'counter' || placeholder == 'attr' || placeholder == 'url')
   {
	   type = "both";
	   placeholder = {prefix:placeholder +"(", suffix:")"};
   }
   return {type:type, data:placeholder};
}

function createOptionSelectAtCaretPosition(textField, opts, cssDef, propertyToReplace, replaceType)
{
   if (opts.length == 0)
   {
       console.debug("No possible completions for: "+ propertyToReplace);
       return;
   }
   if (opts.length == 1 && opts[0] == propertyToReplace)
   {
       console.debug("One completion but it already matches: "+ propertyToReplace);
       return;
   }
   var div = $('#optsContainer');
   var select = $('#optsSelect');
   select.find("option").remove();
   
   for(var i = 0;i<opts.length;i++)
   {
       var option = $("<option>" + opts[i] + "</option>");
       if (i == 0)
           option.attr("selected", "selected");
       select.append(option);
   }
   // .position() uses position relative to the offset parent,
    // so it supports position: relative parent elements
    var pos = $(textField).position();

    // .outerWidth() takes into account border and padding.
    var width = $(textField).outerWidth();

    //show the menu directly over the placeholder

    select.data("handlerData", {cssDef:cssDef, propertyToReplace:propertyToReplace, replaceType:replaceType});
   
    div.css({
       position: "absolute",
       top: pos.top + "px",
       left: (pos.left + width) + "px"
    }).addClass("CodeMirror-completions").show();
    $(select).focus();
}

function handleReplaceAndCaretPos(textField, val, cssDef, propertyToReplace, replaceType)
{
   console.debug("FIRING HANDLEREPLACE");
   if (replaceType == 'PROPERTY')
   {
       textField.focus();
       var colon = cssDef.matchData.matchCaret.colon;
       if (colon == null)
           colon = cssDef.matchData.matchCaret.end;
       textField.caret(colon-propertyToReplace.length, colon);

       val = val + ":";
       var replacement = textField.caret().replace(val);
       textField.val(replacement);

       var diff = val.length - propertyToReplace.length;
       //console.warn("Colon pos: " + colon + " diff: " + diff + " val: " + val.length + " prop: " + propertyToReplace.length)
       textField.caret({start:colon + diff, end:colon + diff, text:"" });
   }
   else
   {
       textField.focus();
       var colon = cssDef.matchData.matchCaret.colon;
       if (colon == null)
           colon = cssDef.matchData.matchCaret.end;
       textField.caret(colon+1, colon+1+propertyToReplace.length);
       
       if (val[val.length] != ";")
       val = val + ";";
       var replacement = textField.caret().replace(val);
       textField.val(replacement);

       textField.caret({start:colon + 1 + val.length, end:colon + 1 + val.length, text:"" });
   }
   $('#optsContainer').hide();
   textField.focus();
}

function findIndicesOf(input, search)
{
   var indices = [];
   while(input.lastIndexOf(search) > -1)
   {
       var index = input.lastIndexOf(search);
       indices[indices.length] = index;
       input = input.substring(0, index);
   }
   return indices.reverse();
}