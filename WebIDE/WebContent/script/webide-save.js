function ctrlS()
{
	console.warn('FIRED CTRLS');
}
function save()
{
	//saveLocalStorage, saveViaBrowser, saveAndGetFile(), saveInServerDB
	if (window.storageInfo = window.storageInfo || window.webkitStorageInfo)
	{
		console.debug('Saving to File System');
		saveAsFile();
	}
	else if (isLocalStorageImplemented())
	{
		console.debug('Saving with local Storage');
		saveLocalStorage();
	}
	
	var submitObj = {
			config:$.webide.config, 
			html:$.webide.codeEditors.html.getValue(), 
			css:$.webide.codeEditors.css.getValue(),
			js:$.webide.codeEditors.js.getValue()
			};
	$.post('./save.wi', {code:JSON.stringify(submitObj), name:$.webide.currentProjectName, key:$.webide.config.key}, function(project){
		console.warn('Saved code.');
		console.warn(project.key, project.version);
		updateKeyAndVersion(project);
	}, 'json');
		
}

function updateKeyAndVersion(project)
{
	$.webide.config.key = project.key;
	$.webide.config.version = project.version;
	$('#keyDisplay').html(project.key);
	$('#versionDisplay').html(project.version);
}

function saveAsFile()
{
	createFileAndFileSystem('html.txt', $.webide.codeEditors.html.getValue());
	createFileAndFileSystem('js.txt', $.webide.codeEditors.js.getValue());
	createFileAndFileSystem('css.txt', $.webide.codeEditors.css.getValue());
	createFileAndFileSystem('projectName.txt', $.webide.currentProjectName);
	createFileAndFileSystem('config.txt', JSON.stringify($.webide.config));
}

function isLocalStorageImplemented()
{
	if(typeof(Storage)!=="undefined")
	{
		console.warn('Has Local Storage Support');
		return true;
	}
	else
	{
		console.warn('NO Local Storage Support');
		return false;
	}
	
}

function saveLocalStorage()
{
	localStorage.artifactHtml = $.webide.codeEditors.html.getValue();
	localStorage.artifactJs = $.webide.codeEditors.js.getValue();
	localStorage.artifactCss = $.webide.codeEditors.css.getValue();
	localStorage.projectName = $.webide.currentProjectName;
	//TODO:Handle config.
}

function loadExistingCodeEditors()
{
	if ($.browser.webkit)
		loadExistingCodeEditorsFromFileSystem();
	else
		loadExistingCodeEditorsFromLocalStorage();
}

function loadExistingCodeEditorsFromLocalStorage()
{
	var html = localStorage.artifactHtml;
	var js = localStorage.artifactJs;
	var css = localStorage.artifactCss;
	var name = localStorage.projectName;
	if (html)
	{
		$.webide.codeEditors.html.setValue(html);
		$.webide.codeEditors.js.setValue(js);
		$.webide.codeEditors.css.setValue(css);
		console.warn('Loading name of project: ' + name);
		if (name != undefined)
		{
			$.webide.currentProjectName = name;
			$('#projectName').val(name);
		}
	}
	else
	{
		console.warn('Found nothing in local Storage!: ', html);
	}
}

function emptyStorage()
{
	localStorage.artifactHtml = "";
	localStorage.artifactJs = "";
	localStorage.artifactCss = "";
	localStorage.projectName = "";
	
	removeAllFiles();
}

function loadExistingCodeEditorsFromFileSystem()
{
	fsCall(function(){getAllFiles();});
}
