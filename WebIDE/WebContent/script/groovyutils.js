ArrayUtils = {};

ArrayUtils._evalClosure = function(closure, closureArg)
{
   var closureFunction;
   if (typeof(closure) === "string")
   {
       closureFunction = function(it)//'it' becomes an implicit variable in the expression
       {
           return eval(closure);
       };
   }
   else
   {
       closureFunction = closure;
   }
   return closureFunction(closureArg);
};

ArrayUtils.each = function(array, closure)
{
   if (array)
   {
       for (var i = 0; i < array.length; i++)
       {
           ArrayUtils._evalClosure(closure, array[i]);
       }
       return array;
   }
   return null;
};

ArrayUtils.reverseEach = function(array, closure)
{
   if (array)
   {
       for (var i = array.length - 1; i >= 0; i--)
       {
           ArrayUtils._evalClosure(closure, array[i]);
       }
       return array;
   }
   return null;
};

ArrayUtils.collect = function(array, closure)
{
   if (array)
   {
       var toReturn = [];
       for (var i = 0; i < array.length; i++)
       {
           toReturn.push(ArrayUtils._evalClosure(closure, array[i]));
       }
       return toReturn;
   }
   return null;
};

ArrayUtils.find = function(array, closure)
{
   if (array)
   {
       for (var i = 0; i < array.length; i++)
       {
           if (ArrayUtils._evalClosure(closure, array[i]))
               return array[i];
       }
   }
   return null;
};

ArrayUtils.findAll = function(array, closure)
{
   if (array)
   {
       var toReturn = [];
       for (var i = 0; i < array.length; i++)
       {
           if (ArrayUtils._evalClosure(closure, array[i]))
               toReturn.push(array[i]);
       }
       return toReturn;
   }
   return null;
};

ArrayUtils.findIndexOf = function(array, closure)
{
   if (array)
   {
       for (var i = 0; i < array.length; i++)
       {
           if (ArrayUtils._evalClosure(closure, array[i]))
               return i;
       }
       return -1;
   }
   return -1;
};

ArrayUtils.indexOf = function(array, it)
{
   if (array)
   {
       for (var i = 0; i < array.length; i++)
       {
           if (array[i] === it)
               return i;
       }
       return -1;
   }
   return -1;
};

ArrayUtils.contains = function(array, it)
{
   return ArrayUtils.indexOf(array, it) != -1;
};

ArrayUtils.intersect = function(array1, array2)
{
   var toReturn = [];
   if (array1 && array2)
   {
       for (var i = 0; i < array1.length; i++)
       {
           if (!ArrayUtils.contains(toReturn, array1[i]) && ArrayUtils.contains(array2, array1[i]))
               toReturn.push(array1[i]);
       }
   }
   return toReturn;
};

