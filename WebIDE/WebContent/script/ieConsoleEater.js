if (window.console == undefined) 
{
	window.console = {
			debug:function(){},
			info:function(){},
			warn:function(){},
			error:function(){},
			log:function(){},
			assert:function(){},
			dir:function(){},
			dirxml:function(){},
			trace:function(){},
			group:function(){},
			groupEnd:function(){},
			time:function(){},
			timeEnd:function(){},
			profile:function(){},
			profileEnd:function(){},
			count:function(){}};
}