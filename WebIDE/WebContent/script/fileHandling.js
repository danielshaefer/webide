function createFileAndFileSystem(filename, content)
{
	try
	{
		window.requestFileSystem  = window.requestFileSystem || window.webkitRequestFileSystem;
		window.storageInfo = window.storageInfo || window.webkitStorageInfo;
		console.warn(window);
		if ($.webide.fs)
		{
			console.warn('already has file system: ' + $.webide.fs.name);
			createFile(filename, content);
		}
		else
		{
			window.storageInfo.requestQuota(window.PERSISTENT, 1024*1024, function(grantedBytes) {
				window.requestFileSystem(window.PERSISTENT, grantedBytes, function(fs){onInitFs(fs, filename, content)}, fsErrorHandler);
			}, function(e) {
				console.log('Error', e);
			});
		}
	}
	catch(ex)
	{
		console.warn('Does not support file writing.');
	}
	//window.requestFileSystem(window.TEMPORARY, 1*1024*1024 /*1MB*/, onInitFs, fsErrorHandler);
}

function fsCall(callback)
{
	try
	{
		window.requestFileSystem  = window.requestFileSystem || window.webkitRequestFileSystem;
		window.storageInfo = window.storageInfo || window.webkitStorageInfo;
		console.warn(window);
		if ($.webide.fs)
		{
			console.warn('already has file system: ' + $.webide.fs.name);
			callback();
		}
		else
		{
			window.storageInfo.requestQuota(window.PERSISTENT, 1024*1024, function(grantedBytes) {
				window.requestFileSystem(window.PERSISTENT, grantedBytes, function(fs){fileSystemSuccessCallback(fs, callback);}, fsErrorHandler);
			}, function(e) {
				console.log('Error', e);
			});
		}
	}
	catch(ex)
	{
		console.warn('Does not support file writing.');
	}
}

function fileSystemSuccessCallback(fs, callback)
{
	console.warn('file system created: ' + fs.name);
	$.webide.fs = fs; //assumes we have some global type space for the file system that all fs calls should be aware of.
	callback();
}

function onInitFs(fs, filename, content) {
	$.webide.fs = fs;
	console.log('Opened file system: ' + fs.name);
	setTimeout(function(){createFile(filename, content);}, (0.01));
}
function fsErrorHandler(e) {
	var msg = '';

	switch (e.code) {
	case FileError.QUOTA_EXCEEDED_ERR:
		msg = 'QUOTA_EXCEEDED_ERR';
		break;
	case FileError.NOT_FOUND_ERR:
		msg = 'NOT_FOUND_ERR';
		break;
	case FileError.SECURITY_ERR:
		msg = 'SECURITY_ERR';
		break;
	case FileError.INVALID_MODIFICATION_ERR:
		msg = 'INVALID_MODIFICATION_ERR';
		break;
	case FileError.INVALID_STATE_ERR:
		msg = 'INVALID_STATE_ERR';
		break;
	default:
		msg = 'Unknown Error';
	break;
	};

	console.log('Error: ' + msg);
}
function createFile(filename, content)
{
	console.warn('Calling Create file');
	var fs = $.webide.fs;
	console.warn(fs.root);
	fs.root.getFile(filename, {create: true}, function(fileEntry) {

	    // Create a FileWriter object for our FileEntry (log.txt).
	    fileEntry.createWriter(function(fileWriter) {

	      fileWriter.onwriteend = function(e) {
	        console.log('Write completed.');
	      };

	      fileWriter.onerror = function(e) {
	        console.log('Write failed: ' + e.toString());
	      };

	      // Create a new Blob and write it to file.
	      var bb = new WebKitBlobBuilder(); // Note: window.WebKitBlobBuilder in Chrome 12.
	      bb.append(content);
	      fileWriter.write(bb.getBlob('text/plain'));
	      console.warn('Done writing file: ' + filename)

	    }, fsErrorHandler);

	  }, fsErrorHandler);

}

function getFile(filename)
{
	var fs = $.webide.fs;
	fs.root.getFile(filename, {}, function(fileEntry) {

	    // Get a File object representing the file,
	    // then use FileReader to read its contents.
	    fileEntry.file(function(file) {
	       var reader = new FileReader();

	       reader.onloadend = function(e) {
	         var txtArea = document.createElement('textarea');
	         txtArea.value = this.result;
	         document.body.appendChild(txtArea);
	       };

	       reader.readAsText(file);
	    }, errorHandler);

	  }, errorHandler);
}

function getAllFiles()
{
	var fs = $.webide.fs;
	var dirReader = fs.root.createReader();
	var entries = [];

	// Call the reader.readEntries() until no more results are returned.
	var readEntries = function() {
		dirReader.readEntries (function(results) {
			if (!results.length) {
				listResults(entries.sort());
			} else {
				entries = entries.concat(toArray(results));
				readEntries();
			}
		}, fsErrorHandler);
	};

	readEntries(); // Start reading dirs.
}

function toArray(list) {
	return Array.prototype.slice.call(list || [], 0);
}

function listResults(entries) {
	entries.forEach(function(entry, i) {
		var dir = entry.isDirectory;
		var file = entry.isFile;
		var name = entry.name;
		//console.warn("File: " + name);
		//console.warn(entry);
		readFile(name, function(content){
			if (name == 'html.txt')
				$.webide.codeEditors.html.setValue(content);
			else if (name == 'js.txt')
				$.webide.codeEditors.js.setValue(content);
			else if (name == 'css.txt')
				$.webide.codeEditors.css.setValue(content);
			else if (name == 'projectName.txt')
			{
				$.webide.currentProjectName = content;
				$('#projectName').val(content);
			}
			else if (name == 'config.txt')
			{
				console.warn('Not yet handling config reloading.');
			}
				
		});
	});
}

function readFile(filename, callback)
{
	var fs = $.webide.fs;
	fs.root.getFile(filename, {}, function(fileEntry) {

	    // Get a File object representing the file,
	    // then use FileReader to read its contents.
	    fileEntry.file(function(file) {
	       var reader = new FileReader();

	       reader.onloadend = function(e) {
	    	 //console.warn('read file with content: ' + this.result);
	         callback(this.result);
	       };

	       reader.readAsText(file);
	    }, fsErrorHandler);

	  }, fsErrorHandler);
}

function removeAllFiles()
{
	try
	{
	var fs = $.webide.fs;
	var dirReader = fs.root.createReader();
    dirReader.readEntries(function(entries) {
      for (var i = 0, entry; entry = entries[i]; ++i) {
        if (entry.isDirectory) {
          entry.removeRecursively(function() {}, errorHandler);
        } else {
          entry.remove(function() {}, errorHandler);
        }
      }
      filelist.innerHTML = 'Directory emptied.';
    }, errorHandler);
	}
	catch(ex)
	{
		console.warn('could not delete all files: ', ex);
	}
}



