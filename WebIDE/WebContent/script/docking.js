function undock()
{
	$.webide.config.undock = true;
	$.webide.config.onundocksrcOpen = !$.webide.eastLayout.state.east.isClosed;
	$.webide.config.onundockpreviewOpen = !$.webide.outerLayout.state.east.isClosed;
	$.webide.eastLayout.close("east");
	$.webide.outerLayout.close("east");
	updatePreview($.webide.codeEditors, true);
	$('#undockPreview').hide();
	$('#dockPreview').show();
}

function dock()
{
	$.webide.config.undock = false;
	updatePreview($.webide.codeEditors, true);
	$.webide.previewWindow.close();
	$.webide.previewWindow = null;
	$.webide.outerLayout.open("east");
	if ($.webide.config.onundocksrcOpen)
		$.webide.eastLayout.open("east");
	$('#dockPreview').hide();
	$('#undockPreview').show();
}